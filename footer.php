<div class="container-fluid backgroundImage footer" style="background-color:rgba(79,164,198,1)">
    <div class="row p-5" > 
        <div  class="col-md-4 border-right border-white">
            <img id="mobileFooter" src="img/logoCompletoFooter.svg" class="text-right" style="width:100%; height:auto" alt="">
        </div>
        <div class="col-md-4 text-center text-white border-right border-white sizeFooter" >
            <div class ="footerText border-bot">
            <p >
                CALLE DIRECCIÓN 543
            </p>

            <p >
                CABA, ARGENTINA.
            </p>
            <p >
                (011) 4545.9809
            </p >
            <p >
                INFO@MAILFUNDACION.COM.AR
            </p >
            </div>
        </div>
        
        <div  class="text-center col-md-4 row m-0 p-0 py-5 icons">  
        <img  src="img/fb_img.svg" class="circle" id="marginLeft" >
        <img  src="img/ig_img.svg" class="circle" >
        <img  src="img/youtube_img.svg" class="circle" >
        <img  src="img/linkedin_img.svg" class="circle" >
            </div>
        </div>
        
	</div>
        <div class="row">
            <div  class="col-md-12 p-2 text-center text-capitalize mobileFooterr" style="background-color:rgba(237,237,237,1) ">
            FUNDACIÓN HÉROES DE LA PATRIA 2020
            </div>
        </div>
    </div>  
		
   <div class ="backgroundFooter" >
        <div class ="backgroundFooter2"> 
            <img  src="img/crosada.svg" style="width:100%; opacity:.2" alt="">
        </div>
    </div> 
  
</div>
