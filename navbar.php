<nav class="navbar navbar-expand-md navbar-light  bg-light ">
    <a class="navbar-brand " href="/heroes/index.php">
        <img id="navbarImage" src="img/logoNavbar.svg"  class="d-inline-block align-top" alt="" loading="auto">
    </a>
    <button class="navbar-toggler first-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent20"
    aria-controls="navbarSupportedContent20" aria-expanded="false" aria-label="Toggle navigation">
    <div class="animated-icon1"><span></span><span></span><span></span></div>
  </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent20">
        <ul class="navbar-nav navbar-nav  mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">LA FUNDACIÓN <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">PROYECTOS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/heroes/novedades.php">NOVEDADES</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">CONTACTO</a>
            </li>
        </ul>
    </div>
</nav>