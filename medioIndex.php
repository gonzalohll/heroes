<div class="container">
    <div class="container-fluid">
        <div class="row my-5">
            <div class="col-md-4 m-0 p-0 imageMedioIndex">
                <img src="img/selloMedio.svg" style="width:100%" alt="Sello fundación" class="img-fluid ">
            </div>
            <div class="col-md-8" id="mobileMedio">
                <h5 id="mobileMedioIndex" class="text-justify" style="font-size:1.5em; font-weight:400; color:#84847c">
                    Fundación Héroes de la Patria es una organización independiente,
                    apartidaria y sin fines de lucro que pretende desarrollar y potenciar la
                    figura y valores de aquellos personajes históricos que hicieron de la
                    República Argentina un territorio pujante y receptivo de todos los
                    habitantes del mundo. Desarrollamos programas que logren el
                    fortalecimiento de su imagen y obra, a través de vínculos cooperativos y
                    solidarios entre los diferentes actores sociales, públicos y privados.
                </h5>
            </div>
        </div>
    </div>
</div>