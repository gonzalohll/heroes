<div class="container-fluid my-0 py-0" style="background-color: rgba(240,240,240,1">
    <div class="container">
        <div class="row pb-5">
            <div class="col-md-12 row text-center" >
                <h1 id="mobileProyectos" class="text-celeste mx-auto mt-2 ">
                    <img src="img/firuleteIzq.svg" style="width:6%" alt="">
                    Proyectos
                    <img src="img/firuleteDer.svg" style="width:6%" alt="">
                </h1>
            </div>
            <div class="col-md-12 text-center">
                <h5 id="mobileProyectos2" class="mx-auto my-3" style="font-size:1.5em">
                    HISTORIAS QUE LLEGAN DIRECTO AL CORAZÓN
                </h5>
            </div>
            <div id="mobileProyectos3" class="text-center mx-auto my-1 col-md-12" >
                <img id="mobileImage" src="img/neneBandera.jpg" alt="">
            </div>
            <div id="mobileVerMas2"class="col-md-9 col-offset-3 mx-auto p-5 ">
                <a href="#" id="mobileVerMas" class="ml-5 px-5 py-3 btn-celeste" style="letter-spacing:2px; font-size:1.1em"> VER MÁS         ></a>
            </div>
        </div>
    </div>
</div>