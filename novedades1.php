<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Novedades</title>
    <?
      include 'topSeteos.php';
    ?>
    <link rel="stylesheet" href="css/novedades1.css" />
</head> 
<body>
  <? include 'navbar.php';?>

  <div class="main-novedades1-container">

    <div class="body-container">

      <div style= "background-color:#ebebeb; width:100%; padding-bottom:50px ; padding-top:50px;padding-left:30px; margin-bottom:30px;">
        <h6>  06-08-2020 </h6>
        <h2>#ObjetosConHistoria</h2>
      </div>

      <div class="main-columns-container">
        <!-- Columna izquierda -->

        <div class="left-column-container">

          <div class="left-column-body">
            <section style="text-align:justify">
              <article>
                <p>Mi nombre es Carlos y quiero contarles la historia de esta réplica que me dejó mi tío abuelo Ernesto, quien fue Granadero allá por la década del 60. Él siempre estuvo orgulloso de pertenecer al Regimiento de Caballería y lo llevó con honor hasta los últimos días de su vida. Era gran admirador de San Martín, y como buen salteño y patriota, también lo era del General Martín Miguel de Güemes. </p>

                <p>Un poco por vocación, otro poco por la admiración que yo sentía por él, decidí tomar el mismo camino y viajé a Buenos Aires para ingresar al Colegio Militar. Unos años después, tuve el honor de regresar a nuestra querida provincia y fuimos junto con Ernesto a rendir homenaje a Güemes, al pie de su monumento. </p>

                <p>Unos días después, en una cena familiar, me obsequió la réplica de Granadero. 'Lo guardé siempre como un tesoro, ahora quiero que lo tengas vos, para continuar con este legado sanmartiniano', fueron sus palabras. Hoy es mi tesoro más preciado. Este es mi objeto con historia".</p>

              </article>


            </section>

            <img src="./img/icons/escarapela.png" /> <span> Orgullosos de nuestros héroes </span>

            <div class="big-image"></div>

            <!-- Carousel -->

            <div id="principal">
                <div class="col-12 text-center">
                    <h3>  </h3>
                </div>

                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel"  data-interval="false" >
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-container">
                        <div class="carousel-inner custom-inner">
                            <div class="carousel-item active img-fluid">
                                <div class="container-card-deck">
                                      <img class="carousel-image" src="./img/Novedades_2.png" />
                                      <img class="carousel-image" src="./img/Novedades_2.png" />
                                      <img class="carousel-image" src="./img/Novedades_2.png" />
                                </div>
                            </div>

                            <div class="carousel-item img-fluid">
                                <div class="container-card-deck">
                                      <img class="carousel-image" src="./img/Novedades_2.png" />
                                      <img class="carousel-image" src="./img/Novedades_2.png" />
                                      <img class="carousel-image" src="./img/Novedades_2.png" />
                                </div>
                            </div>

                            <div class="carousel-item img-fluid">
                                <div class="container-card-deck">
                                      <img class="carousel-image" src="./img/Novedades_2.png" />
                                      <img class="carousel-image" src="./img/Novedades_2.png" />
                                      <img class="carousel-image" src="./img/Novedades_2.png" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Botones-->

                    <a class="carousel-control-prev " href="#carouselExampleCaptions " role="button " data-slide="prev ">
                        <span class="carousel-control-prev-icon " aria-hidden="true "></span>
                        <span class="sr-only ">Previous</span>
                    </a>
                    <a class="carousel-control-next " href="#carouselExampleCaptions " role="button " data-slide="next ">
                        <span class="carousel-control-next-icon " aria-hidden="true "></span>
                        <span class="sr-only ">Next</span>
                    </a>
                </div>
            </div>

          </div>
        </div>

        <div class="vl"></div>



        <!-- Columna derecha -->

        <div class="right-column-container">
          <div style="background-color:#99CCFF; color:white; ">
            <h3 >OTRAS NOVEDADES</h3>
          </div>

          <div class="novedades-cards-container">

            <div class="novedades-card">
              <img class="novedades-image" src="./img/Novedades_3.png" />
              <div style="background-color: #ebebeb">
              <p> ANIVERSARIO DEL FALLECIMIENTO DEL GRAL. BALCARCE </p>
              </div>
            </div>

            <div class="novedades-card">
              <img class="novedades-image" src="./img/Novedades_4.png" />

            </div>

            <div class="novedades-card">
              <img class="novedades-image" src="./img/Novedades_3.png" />

            </div>

          </div>

        </div>

      </div>

    </div>
  </div>
  <?include 'footer.php';
    include 'bottomSeteos.php';
  ?>

</body>
</html>