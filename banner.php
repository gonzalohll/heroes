<div class="m-0 p-0 container-fluid position-relative embed-responsive-21by9 imagemFundo">
     <video class="embed-responsive-item" width="100%" muted loop autoplay>
        <source src="img/banderaFlameando.mp4" type="video/mp4">
    </video>
    <div class="position-absolute text-right text-white px-3" style="border-right:5px solid white;text-shadow: 1px 1px 2px rgba(0,0,0,0.32); box-shadow: 2px 0px 0px 0px rgba(0,0,0,0.32); bottom:2em;right:3em "id="mobile" > 
        <h5 id="mobile" style="font-size:1.8em">NUESTRO VIAJE ES PANORÁMICO,</h5>
        <p id="mobile"class="p-0 my-1 mx-0" style="font-size:1.2em">MIRA EL PASADO, REVISA EL PRESENTE</p>
        <p id="mobile"class="p-0 my-1 mx-0" style="font-size:1.2em"> Y SUEÑA CON EL FUTURO.</p>
    </div>
</div>