<div class="container-fluid bg-darker py-4">
    <div class="container bg-darker text-light py-4">
        <div class="row">
            <div class="col-md-4 px-4">
                <h4 class="tituloFooter">FUNDACIÓN HÉROES DE LA PATRIA</h4>
                <p>El compromiso de nuestra fundación es contribuir con acciones concretas a la Educación y la Cultura de nuestra Nación</p>
            </div>
            <div class="col-md-4 px-4">
                <h4 class="tituloFooter">CONTACTANOS</h4>
                <p>Dirección: ....</p>
                <p>Teléfono: ....</p>
            </div>
            <div class="col-md-4 px-4">
                <h4 class="tituloFooter">REDES SOCIALES</h4>
                <i class="fab fa-2x mx-2 iconoRedesFooter fa-facebook iconoFooter"></i>
                <i class="fab fa-2x mx-2 iconoRedesFooter fa-instagram iconoFooter"></i>
                <i class="fab fa-2x mx-2 iconoRedesFooter fa-twitter-square iconoFooter"></i>
            </div>
            <div class="col-md-12 text-center">
                <img src="img/logoFooter.svg" class="mx-auto" style="width:50%" alt="">
            </div>
        </div>
    </div>
</div>