<div class="container-fluid text-white bg-dark mt-3 pb-4">
    <div class="row px-3 py-4">
        <div class="col-md-2 offset-md-1 d-none d-sm-block">
            <img src="img/separadorIzq.svg" style="width:100%" alt="">
        </div>
        <div class="col-md-6 text-center text-capitalize" style="font-family: 'Times New Roman', Times, serif;font-weight:400">
            <h4>HISTORIAS QUE LLEGAN DIRECTO AL CORAZÓN</h4>
        </div>
        <div class="col-md-2 d-none d-sm-block">
            <img src="img/separadorDer.svg" style="width:100%" alt="">
        </div>
    </div>
    <div class="container">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active embed-responsive embed-responsive-21by9">
                    <iframe style="width:100%;" class="embed-responsive-item" src="https://www.youtube.com/embed/DkFJE8ZdeG8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="carousel-item">
                    <img src="https://via.placeholder.com/210x90?text=Imagen+ +temporal+ +21x9" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span>
                    <img src="img/flechaIzq.svg" alt="">
                </span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span>
                    <img src="img/flechaDer.svg" alt="">
                </span>
                <span class="sr-only">Siguiente</span>
            </a>
        </div>
    </div>
</div>