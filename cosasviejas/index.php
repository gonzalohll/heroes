<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fundación Héroes de la Patria</title>
    <?php
        include'topSeteos.php';
    ?>

    
</head>
<body>
    <?php
        include'navbar.php';
    ?>
    <div class="container-fluid p-0 embed-responsive-21by9">
        <iframe style="width:100%;" class="embed-responsive-item" src="https://www.youtube.com/embed/rM20B4HbrCs?loop=1">
        </iframe>
    </div>
    <?php
        include'botonera.php';
        include'bottomCarousel.php';
        include'footer.php';
        include'bottomSeteos.php';
    ?>
</body>
</html>