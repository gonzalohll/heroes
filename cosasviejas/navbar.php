<nav class="navbar navbar-expand-md shadow-lg navbar-light bg-light" style="box-shadow: 0px 0px 5px 2px rgba(0,0,0,0.20); z-index:5">
    <a class="navbar-brand ml-1 text-right" style="" href="index.php">
        <div class="container-fluid p-0 pl-4">
            <div class="row">
                <div class="col-md-8 p-0 navbarTitle text-right">
                    FUNDACIÓN
                    <br/>
                    Héroes de la Patria
                </div>
                <div class="col-md-4 align-middle my-auto text-right">
                    <img src="img/logo.svg" style="max-width:55px; width:85%;" alt="">
                </div>
            </div>
        </div>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarPrincipal"
        aria-controls="navbarPrincipal" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbarPrincipal navbar-collapse" id="navbarPrincipal">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link" href="#" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Institucional <span class="sr-only">(current)</span></a>
                <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Nuestra História</a>
                <a class="dropdown-item" href="#">Misión, visión y valores</a>
                <a class="dropdown-item" href="#">Nuestro equipo</a>               
            </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Acciones <span class="sr-only">(current)</span></a>
                <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">San Martin y Belgrano a las Escuelas</a>
                <a class="dropdown-item" href="#">Contanos tu proyecto para hacer Patria</a>
                <a class="dropdown-item" href="#">Proyecto de Esculturas</a>
                <a class="dropdown-item" href="#">230 Anos del paso a la inmortalidad</a>
               
            </div>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="resultados.php?ofertas=1">Donaciones <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="resultados.php?ofertas=1">Noticias <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="resultados.php?ofertas=1">Voluntariado <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="resultados.php?ofertas=1">Contacto <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link" href="#" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login <span class="sr-only">(current)</span></a>
                <div class="dropdown-menu teste" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Registrarse</a>
                <a class="dropdown-item" href="#">Iniciar Cesión</a>
            </div>
            </li>
        </ul>
    </div>
</nav>