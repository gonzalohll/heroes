<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fundación Héroes de la patria</title>
    <?
        include 'topSeteos.php';
    ?>
</head>
<body>
    <?
        include 'navbar.php';
        include 'banner.php';
        include 'medioIndex.php';
        include 'escarapela.php';
        include 'proyectosIndex.php';
        include 'novedadesIndex.php';
        include 'footer.php';
        include 'bottomSeteos.php';
    ?>
<script>$(document).ready(function () {

$('.first-button').on('click', function () {

  $('.animated-icon1').toggleClass('open');
});
$('.second-button').on('click', function () {

  $('.animated-icon2').toggleClass('open');
});
$('.third-button').on('click', function () {

  $('.animated-icon3').toggleClass('open');
});
});</script>
    
</body>
</html>