<div class="container-fluid my-0 py-0 space" style="position:relative">
    <img src="img/logoNew.png" class="m-0 p-0 imageLogo"  alt="">
    
        <div  class="row pb-5">
            <div class="col-md-12 row text-center">
                <h1 id="mobileProyectos" class="text-celeste mx-auto mt-5 mb-5">
                    <img src="img/firuleteIzq.svg" style="width:6%" alt="">
                    Novedades
                    <img src="img/firuleteDer.svg" style="width:6%" alt="">
                </h1>
            </div>
            <div  class="col-md-12 row justify-content-around">
                <div class="col-md-3  border border-secondary p-0">
                    <div class="col-md-12 p-0">
                        <img src="img/placeholderNovedad.jpg" class="img-fluid image-novedad"  alt="">
                    </div>
                    <div class="col-md-12 itemNovedad text-center">
                        <p class="tituloNovedad itemNovedad">TÍTULO DE LA NOVEDAD</p>
                        <p class="textoNovedad">
                            Este es un texto de referencia. Contenido
                            de la novedad de aproximádamente dos o
                            tres renglones.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 border border-secondary p-0">
                    <div class="col-md-12 p-0">
                        <img src="img/novedad2.png" class="img-fluid image-novedad" alt="">
                    </div>
                    <div class="col-md-12 itemNovedad text-center">
                        <p class="tituloNovedad itemNovedad">TÍTULO DE LA NOVEDAD</p>
                        <p class="textoNovedad">
                            Este es un texto de referencia. Contenido
                            de la novedad de aproximádamente dos o
                            tres renglones.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 border border-secondary p-0">
                    <div class="col-md-12 p-0">
                        <img src="img/novedad3.png" class="img-fluid image-novedad"  alt="">
                    </div>
                    
                    <div class="col-md-12 itemNovedad text-center">
                        <p class="tituloNovedad itemNovedad">TÍTULO DE LA NOVEDAD</p>
                        <p class="textoNovedad">
                            Este es un texto de referencia. Contenido
                            de la novedad de aproximádamente dos o
                            tres renglones.
                        </p>
                    </div>
                </div>
            </div>
        </div>
</div>